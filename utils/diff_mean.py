import numpy as np
diffs = [0.6545155706595811, 0.6531438615762886, 0.6586142565925629, 0.6549337803250197]
diff = np.mean(diffs)
diff_file = "../save_variable/diff_1.npz"
print("diff mean is {} rad".format(diff))
np.savez(diff_file, diff=diff)