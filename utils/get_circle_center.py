#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import sys
import signal
import argparse
import paho.mqtt.client as mqtt
import json
import time
import numpy as np
import os
from scipy import optimize
import functools
import matplotlib.pyplot as plt
import logging

idx = 0
POINTS_NUM = 3600
points_array = np.ndarray(shape=(2, POINTS_NUM), dtype=np.float)
points_array.fill(0)
center = np.ndarray(shape=(2, 1), dtype=np.float)
center.fill(0)
running = True

center_found = False
offset_found = False

data_file = "../save_variable/circle_test.npz"
logger = logging.getLogger(__name__)

# Find circle center
def countcalls(fn):
    "decorator function count function calls "

    @functools.wraps(fn)
    def wrapped(*args):
        wrapped.ncalls +=1
        return fn(*args)

    wrapped.ncalls = 0
    return wrapped

def calc_R(xc, yc):
    """ calculate the distance of each 2D points from the center (xc, yc) """
    return np.sqrt(np.sum((points_array - np.array([[xc], [yc]])) ** 2, axis=0))

@countcalls
def f_2(c):
    """ calculate the algebraic distance between the 2D points and the mean circle centered at c=(xc, yc) """
    Ri = calc_R(*c)
    return Ri - Ri.mean()



# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("socket_mqtt")


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    global idx, points_array, center_found, center, offset_found, running
    if not running:
        return
    try:
        data = json.loads(msg.payload)
    except ValueError as e:
        # logger.error("Value error. Message is: {}".format(msg.payload))
        return

    # print("Time: {}.".format(cur_time))
    x = float(data["x"])
    y = float(data["y"])

    points_array[:, idx] = [x, y]

    if idx >= POINTS_NUM-1:  # terminating condition
        if not center_found:
            mean = np.mean(points_array, axis=1)
            x_m = mean[0]
            y_m = mean[1]
            center_estimate = x_m, y_m
            center, ier = optimize.leastsq(f_2, center_estimate)

            xc_2, yc_2 = center
            Ri_2 = calc_R(xc_2, yc_2)
            R_2 = np.mean(Ri_2)
            print("Center is: [{}, {}]".format(xc_2, yc_2))
            print("Radius is: {}".format(R_2))
            plt.scatter(points_array[0, :], points_array[1, :], label="path")
            plt.plot(xc_2, yc_2, "ro")
            plt.legend()  # 展示图例
            plt.show()  # 显示图形
            idx = 0
            points_array.fill(0)
            running = False
            center_found = True
            # np.savez(os.path.abspath(data_file), robot_front=robot_front, points_array=points_array, quat_array=quat_array)
            # np.savez(os.path.abspath(diff_file), diff=diff)
        else:
            mean = np.mean(points_array, axis=1)
            x_m = mean[0]
            y_m = mean[1]
            plt.scatter(points_array[0, :], points_array[1, :], label="path")
            plt.plot(x_m, y_m, "go")
            plt.legend()  # 展示图例
            plt.show()  # 显示图形
            print("Still point: [{}, {}]".format(x_m, y_m))
            diff = np.array([[x_m, y_m]]) - center
            print("Change in Motive Rigid Body Pivot Point Translation")
            print("x: {}".format(-diff[0, 0] * 1000))
            print("y: 0")
            print("z: {}".format(diff[0, 1] * 1000))
            running = False
            offset_found = True

        return
    else:  # get new data
        pass

    if idx % 100 == 0:
        print("idx: {}".format(idx))
    idx += 1


if __name__ == "__main__":
    def signal_handler(sig, frame):
        print('Exiting...')
        client.disconnect()
        sys.exit(0)


    signal.signal(signal.SIGINT, signal_handler)

    parser = argparse.ArgumentParser(description="Robot circle finder. ")
    parser.add_argument("-v", "--verbose", default=False, help="Verbose output. ", action="store_true")

    args = parser.parse_args()
    verbose = args.verbose

    # MQTT
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.enable_logger()
    client.connect("localhost", 1883, 60)

    # circle starts
    client.publish("robot1", json.dumps({"l": -200, "r": 200}))
    client.loop_start()
    while not center_found:
        pass
    running = False

    client.publish("robot1", json.dumps({"l": 0, "r": 0}))
    time.sleep(1)
    # # wait for the robot to cool down
    # i = 3
    # while i > 0:
    #     print("wait for the robot to cool down, {}...".format(i))
    #     time.sleep(1)
    #     i -= 1
    #
    # print("wait to get position mean")
    # running = True
    #
    # while not offset_found:
    #     pass
