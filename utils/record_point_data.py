#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import sys
import signal
import argparse
import paho.mqtt.client as mqtt
import json
import numpy as np
import os
import logging

idx = 0
POINTS = 800
points_array = np.ndarray(shape=(2, POINTS), dtype=np.float)
quat_array = np.ndarray(shape=(4, POINTS), dtype=np.float)
points_array.fill(0)
quat_array.fill(0)
s_sum = np.zeros((1, 3), dtype=np.float)

THRESHOLD = 1  # m

data_file = "../save_variable/data_record.npz"

finished = False

logger = logging.getLogger(__name__)


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("socket_mqtt")


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    global idx, points_array, finished, quat_array
    if finished:
        return
    try:
        data = json.loads(msg.payload)
    except ValueError as e:
        # logger.error("Value error. Message is: {}".format(msg.payload))
        return

    # print("Time: {}.".format(cur_time))
    x = float(data["x"])
    y = float(data["y"])
    z = float(data["z"])

    qw = float(data["qw"])
    qx = float(data["qx"])
    qy = float(data["qy"])
    qz = float(data["qz"])

    points_array[:, idx] = [x, y]
    quat_array[:, idx] = [qw, qx, qy, qz]

    if idx % 100 == 0:
        print("idx: {}".format(idx))
    if idx >= POINTS-1:
        finished = True
        return
    idx += 1

def save_data():
    global idx, points_array, finished, quat_array
    points_array = points_array[:, 0:idx]
    quat_array = quat_array[:, 0:idx]

    np.savez(os.path.abspath(data_file), points_array=points_array, quat_array=quat_array)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Robot circle finder. ")
    parser.add_argument("-v", "--verbose", default=False, help="Verbose output. ", action="store_true")

    args = parser.parse_args()
    verbose = args.verbose

    # MQTT
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.enable_logger()
    client.connect("localhost", 1883, 60)

    def signal_handler(sig, frame):
        global finished
        print('Exiting...')
        finished = True

    signal.signal(signal.SIGINT, signal_handler)

    # circle starts
    client.loop_start()
    while not finished:
        pass
    save_data()
    client.loop_stop()
    exit(0)
