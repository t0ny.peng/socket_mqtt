#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import matplotlib.pyplot as plt
import numpy as np
from mathutils import quaternion_to_euler_angle
from pykalman import KalmanFilter

data_file = "../save_variable/data_record.npz"
if __name__ == "__main__":
    npzfile = np.load(data_file)
    points_array = npzfile["points_array"]
    quat_array = npzfile["quat_array"]
    k, b = np.polyfit(points_array[0, :], points_array[1, :], 1)

    x_range = np.linspace(min(points_array[0, :]), max(points_array[0, :]), 10)
    y = k * x_range + b
    plt.plot(points_array[0, :], points_array[1, :], 'r-',
             x_range, y, 'g:')
    plt.show()  # 显示图形

    eulers = quaternion_to_euler_angle(quat_array[0], quat_array[1], quat_array[2], quat_array[3])
    # eulers = (eulers + 2*np.pi) % (2*np.pi)
    eulers = np.rad2deg(eulers)

    plt.plot(eulers[2, :], label="z")
    plt.legend()  # 展示图例
    plt.show()  # 显示图形
